This is my online resume website created using Javascript, HTML, and CSS.

The site can be viewed by this link: http://www.pages.drexel.edu/~thn42/

The file helper.js plays a very important role in formatting and designing the page.
The file resumeBuilder.js contains my resume information in JSON format and implementations of appending the resume to the page.