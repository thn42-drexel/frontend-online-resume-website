
var bio =
{
	"name": "Tuan Huu Minh Nguyen",
	"role": "Undergraduate Student",
	"contacts":
	{
		"mobile": "202-459-1045",
		"email": "mtuan93@gmail.com",
		"bitbucket": "thn42-drexel",
		"twitter": "@mtuan93",
		"location": "Philadelphia, PA"
	},
	"welcomeMessage": "Welcome to my personal homepage",
	"skills":
	["Java", "Python", "HTML", "CSS", "JavaScript", "Bash scripting", "SourceTree-version control", "ACL2", "Awk"],
	"bioPic": "images/tuan.jpg"
};

var education = 
{
	"schools": 
	[
		{
			"name": "Drexel University",
			"location": "Philadelphia, PA",
			"degree": "Bachelor in Science of Computer Science",
			"gpa": "N/A",
			"dates": "September 2014 - Present",
		},
		{
			"name": "Community College of Philadelphia",
			"location": "Philadelphia, PA",
			"degree": "Associate Degree of Computer Science",
			"gpa": "4.00",
			"dates": "September 2012 - May 2014",
		}
	]
};

var work =
{
	"jobs":
	[
		{
			"employer": "Community College of Philadelphia",
			"title": "Math Tutor",
			"dates": "January 2013 to May 2014",
			"description": ["Tutored and assessed students needing supplemental help with schoolwork and study habits in Pre-Calculus, Calculus, Discrete Math, Linear Algebra courses",
							"Assisted students in acquiring better understanding of targeted weak areas within a subject or a subject as a whole",
							"Received Florence Fishman Humanitarian Award for Outstanding Services by a Tutor, Spring 2014"]
		},
		{
			"employer": "Community College of Philadelphia",
			"title": "Teaching Assistant",
			"dates": "January 2013 to May 2014",
			"description": ["Assisted professors in developing lesson plans, conducting increased learning experience for students in Pre-Calculus, Calculus, Discrete Math, Linear Algebra courses"]
		}
	]
};

var projects =
{
	"projects": 
	[
		{
			"title": "Building Personal Webpage",
			"dates": "2014",
			"description": ["Designed and implemented a Javascript helper to format sections of the page",
							"Utilized JQuery, HTML, and CSS to design the page"], 					
			"source": "https://bitbucket.org/thn42-drexel/frontend-online-resume-website"
		},
		{
			"title": "Java Game Development - Bouncing Ball",
			"dates": "2012",
			"description": ["Utilized different Java Libraries to design the game",
							"Applied collision detection techniques, designed and created various types of animations to implement the game"],			
			"source": "https://bitbucket.org/thn42-drexel/java-game-bounce-ball",
			"images": ["images/javaGame1.png","images/javaGame2.png"]
		}
	]
};

var awards =
{
	"awards":
	[
		{
			"title": "Dean's Scholarship",
			"dates": "September 2014 - Present",
			"provider": "Drexel University"
		},
		{
			"title": "Dragon Alumni Scholarship",
			"dates": "September 2014 - Present",
			"provider": "Drexel University"
		},
		{
			"title": "Phi Theta Kappa Scholarship",
			"dates": "September 2014 - Present",
			"provider": "Drexel University"
		},
		{
			"title": "Dragon Scholarship",
			"dates": "September 2014 - Present",
			"provider": "Drexel University"
		},
		{
			"title": "Math Department Award",
			"dates": "May 2014",
			"provider": "Math Department - Community College of Philadelphia"
		},
		{
			"title": "Federation Memorial Scholarship",
			"dates": "May 2014",
			"provider": "Community College of Philadelphia"
		},
		{
			"title": "Florence Fishman Award for Outstanding Tutor Service",
			"dates": "May 2014",
			"provider": "Community College of Philadelphia"
		},
		{
			"title": "Foundation Scholarship",
			"dates": "Fall 2012 - Spring 2014",
			"provider": "Community College of Philadelphia"
		},
		{
			"title": "First Place in Rocket-City Math League",
			"dates": "April 2013",
			"provider": "Mu Alpha Theta"
		},
		{
			"title": "Student Leader of the Year",
			"dates": "Fall 2012 - Spring 2014",
			"provider": "Community College of Philadelphia"
		},
		{
			"title": "Outstanding Service Award",
			"dates": "Fall 2012 - Spring 2014",
			"provider": "Community College of Philadelphia"
		},
		{
			"title": "Outstanding Leader Award",
			"dates": "Fall 2012 - Spring 2014",
			"provider": "Community College of Philadelphia"
		},
		{
			"title": "The Second Prize in the Vietnam Mathematics Olympiads",
			"dates": "2011",
			"provider": "Vietnam"
		},
		{
			"title": "The First Place in the Mathematics Problem Solving Contests",
			"dates": "2011",
			"provider": "Math and Youth Magazine, Vietnam"
		},
		{
			"title": "Two gold medals in the 30/4 Mathematics Olympiads",
			"dates": "2010-2011",
			"provider": "Vietnam"
		}
	]
};
var organizations =
{
	"organizations":
	[
		{
			"name": "Math Club",
			"description": "Community College of Philadelphia",
			"position": "President",
			"dates": "September 2012 - May 2014"
		},
		{
			"name": "Mu Alpha Theta",
			"description": "Community College Mathematics Honor Society",
			"position": "Member",
			"dates": "May 2013 - May 2014"
		},
		{
			"name": "Phi Theta Kappa",
			"description": "International Student Honor Society",
			"position": "Member",
			"dates": "September 2012 - May 2014"
		},
		{
			"name": "Green Cycle Alliance",
			"description": "Community College of Philadelphia",
			"position": "Member",
			"dates": "September 2012 - May 2014"
		}
	]
};

var formattedMobile = HTMLmobile.replace("%data%", bio.contacts.mobile);
var formattedEmail = HTMLemail.replace("%data%", bio.contacts.email);
var formattedBitbucket = HTMLbitbucket.replace("%data%", bio.contacts.bitbucket);
var formattedLocation = HTMLlocation.replace("%data%", bio.contacts.location);
var formattedName = HTMLheaderName.replace("%data%", bio.name);
var formattedRole = HTMLheaderRole.replace("%data%", bio.role);
var formattedBioPic = HTMLbioPic.replace("%data%",bio.bioPic);
var formattedMesg = HTMLWelcomeMsg.replace("%data%",bio.welcomeMessage);

$("#topContacts").append(formattedMobile,formattedEmail,formattedBitbucket,formattedLocation);
$("#header").append(formattedName,formattedRole,formattedBioPic,formattedMesg);

if(bio.skills.length > 0)
{
	$("#header").append(HTMLskillsStart);
	for(skill in bio.skills)
	{
		var formattedSkill = HTMLskills.replace("%data%", bio.skills[skill]);
		$("#skills").append(formattedSkill);
	}
}


function displayWork() 
{
	for(job in work.jobs)
	{
		// create new div for work experience
		$("#workExperience").append(HTMLworkStart);
		// concat employer and title
		var formattedEmployer = HTMLworkEmployer.replace("%data%", work.jobs[job].employer);
		var formattedTitle = HTMLworkTitle.replace("%data%",work.jobs[job].title);
		var formattedEmployerTitle = formattedEmployer + formattedTitle;
		$(".work-entry:last").append(formattedEmployerTitle);

		var formattedDate = HTMLworkDates.replace("%data%",work.jobs[job].dates);
		$(".work-entry:last").append(formattedDate);
		$(".work-entry:last").append("<br>");
		for(description in work.jobs[job].description)
		{
			var formattedDescription = HTMLworkDescription.replace("%data%",work.jobs[job].description[description]);
			$(".work-entry:last").append(formattedDescription);
		}
		//var formattedDescription = HTMLworkDescription.replace("%data%",work.jobs[job].description);
		//$(".work-entry:last").append(formattedDescription);
	}
}
function displaySchool()
{
	for(school in education.schools)
	{
		$("#education").append(HTMLschoolStart);

		var formattedName = HTMLschoolName.replace("%data%",education.schools[school].name);
		var formattedLocation = HTMLschoolLocation.replace("%data%",education.schools[school].location);
		var formattedDegree = HTMLschoolDegree.replace("%data%",education.schools[school].degree);
		var formattedGPA = HTMLschoolGPA.replace("%data%",education.schools[school].gpa);
		var formattedDates = HTMLschoolDates.replace("%data%",education.schools[school].dates);
		$(".education-entry:last").append(formattedName + formattedDegree);
		$(".education-entry:last").append(formattedDates);
		$(".education-entry:last").append(formattedGPA);
		$(".education-entry:last").append(formattedLocation);
		
	}
};
function displayProject()
{
	for (project in projects.projects)
	{
		$("#projects").append(HTMLprojectStart);

		var formattedTitle = HTMLprojectTitle.replace("%data%",projects.projects[project].title);
		var formattedDates = HTMLprojectDates.replace("%data%",projects.projects[project].dates);
		$(".project-entry:last").append(formattedTitle);
		$(".project-entry:last").append(formattedDates);
		$(".project-entry:last").append("<br>");
		for(description in projects.projects[project].description)
		{
			var formattedDescription = HTMLprojectDescription.replace("%data%",projects.projects[project].description[description]);
			$(".project-entry:last").append(formattedDescription);
		}
		for(image in projects.projects[project].images)
		{
			var formattedImage = HTMLprojectImage.replace("%data%",projects.projects[project].images[image]);
			$(".project-entry:last").append(formattedImage);
		}
	}
};
function displayAward()
{
	for(award in awards.awards)
	{
		$("#award").append(HTMLawardStart);

		var formattedTitle = HTMLawardTitle.replace("%data%",awards.awards[award].title);
		var formattedDates = HTMLawardDates.replace("%data%",awards.awards[award].dates);
		var formattedProvider = HTMLawardProvider.replace("%data%",awards.awards[award].provider);
		$(".award-entry:last").append(formattedTitle);
		$(".award-entry:last").append(formattedDates);
		$(".award-entry:last").append(formattedProvider);
	}
};
function displayOrg()
{
	for(org in organizations.organizations)
	{
		$("#organization").append(HTMLorgStart);

		var formattedName = HTMLorgName.replace("%data%",organizations.organizations[org].name);
		var formattedDescription = HTMLorgDescription.replace("%data%",organizations.organizations[org].description);
		var formattedPosition = HTMLorgPosition.replace("%data%",organizations.organizations[org].position);
		var formattedDates = HTMLorgDates.replace("%data%",organizations.organizations[org].dates);
		$(".org-entry:last").append(formattedName + formattedDescription,formattedPosition);
		$(".org-entry:last").append(formattedDates);		
	}
}

$("#mapDiv").append(googleMap);
displayOrg();
displayAward();
displaySchool();
displayWork();
displayProject();

